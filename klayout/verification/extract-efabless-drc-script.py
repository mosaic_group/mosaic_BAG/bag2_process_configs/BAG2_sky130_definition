#! /usr/bin/env python

#
# Sky130 Efabless PDK comes with a comprehensive DRC script,
# but it is wrapped in XML and can therefore not be included using
# %include path.drc
#
# This script will extract the DRC script part that can be included
# and stores it so sky130A_efabless.drc
#

import os
import sys
import xml.etree.ElementTree as ET

if len(sys.argv) == 2:
    xml_drc_script = sys.argv[1]
else:
    xml_drc_script = f"{os.environ['PDK_ROOT']}/{os.environ['PDK']}/libs.tech/klayout/drc/sky130A.lydrc"

output_path = f"{os.environ['BAG_TECH_CONFIG_DIR']}/klayout/verification/sky130A_efabless.drc"

print(f"Converting XML DRC script: {xml_drc_script}")

tree = ET.parse(xml_drc_script)
drc_script = tree.find("./text").text

with open(output_path, "w") as f:
    f.write(drc_script)

print(f"Saved extracted DRC script to {output_path}")