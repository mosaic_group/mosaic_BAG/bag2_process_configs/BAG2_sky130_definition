#! /usr/bin/env python

#
# This script allows to diff multiple klayout report databases
#

from itertools import pairwise
import os
import sys
from typing import *
import xml.etree.ElementTree as ET

if len(sys.argv) == 2:
    print(f"Usage: {sys.argv[0]} <drc-lyrdb-file1> [... <drc-lyrdb-fileN>]")
    sys.exit(1)

lydrb_files = sys.argv[1:]

print(f"Analyzing DRC reports: {lydrb_files}")

rules_by_report_name: Dict[str, list] = {}

for report in lydrb_files:
    report_name = os.path.basename(report)

    tree = ET.parse(report)
    rule_names = [e.text for e in tree.findall("./categories/category/name")]
    rules_by_report_name[report_name] = set(rule_names)

report_pairs = list(pairwise(rules_by_report_name.keys()))
for report_name1, report_name2 in report_pairs:
    rules1 = rules_by_report_name[report_name1]
    rules2 = rules_by_report_name[report_name2]

    diff12 = list(rules1 - rules2)
    diff12.sort()
    diff21 = list(rules2 - rules1)
    diff21.sort()
    intersection = list(rules1.intersection(rules2))
    intersection.sort()
    print(f"Compare {report_name1} ({len(rules1)} rules) VS {report_name2} ({len(rules2)} rules):")
    print(f"  rules only in {report_name1} ({len(diff12)}): {diff12}")
    print(f"  rules only in {report_name2} ({len(diff21)}): {diff21}")
    print(f"  common rules ({len(intersection)}): {intersection}")
