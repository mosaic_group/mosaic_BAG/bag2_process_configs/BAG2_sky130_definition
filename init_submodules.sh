#!/usr/bin/env bash
DIR=$( cd `dirname $0` && pwd )
cd $DIR

#List the submodules here
echo "Initiating submodules in $DIR"
MODULES="\
   xschem_sky130 \
         "
git submodule sync

#Self-made selective recursion
for mod in $MODULES; do
    git submodule update --init
    if [ -f $mod/init_submodules.sh ]; then
        cd $mod
        ./init_submodules.sh
    fi
done

exit 0
