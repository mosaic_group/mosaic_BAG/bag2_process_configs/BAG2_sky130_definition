v {xschem version=3.1.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -70 -60 0 -60 {
lab=PLUS}
N 0 30 0 60 {
lab=MINUS}
N -70 60 0 60 {
lab=MINUS}
N 0 -60 0 -30 {
lab=PLUS}
N -70 -0 -20 0 {
lab=BULK}
C {sky130_fd_pr/res_generic_po.sym} 0 0 0 0 {name=R1
W=w
L=l
model=res_generic_po
spiceprefix=M
mult=1}
C {devices/iopin.sym} -70 -60 0 1 {name=p3 lab=PLUS}
C {devices/iopin.sym} -70 60 0 1 {name=p1 lab=MINUS}
C {devices/iopin.sym} -70 0 0 1 {name=p2 lab=BULK}
