v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N -70 -0 -40 -0 {
lab=G}
N 0 -60 0 -30 {
lab=S}
N 0 30 -0 60 {
lab=D}
N 0 -0 60 0 {
lab=B}
N -70 60 0 60 {
lab=D}
N -70 -60 0 -60 {
lab=S}
C {devices/iopin.sym} -70 60 0 1 {name=p1 lab=D}
C {devices/iopin.sym} -70 0 0 1 {name=p2 lab=G}
C {devices/iopin.sym} -70 -60 0 1 {name=p3 lab=S}
C {devices/iopin.sym} 60 0 0 0 {name=p4 lab=B}
C {sky130_fd_pr/pfet_01v8_lvt_nf.sym} -20 0 0 0 {name=M0
spiceprefix=X

L="'l * 1e6'"
W="(w * 1e6)"
nf=nf
mult=1
ad="'int((nf+1)/2) * W / nf* 0.29'"
pd="'2*int((nf+1)/2) * (W / nf+ 0.29)'"
as="'int((nf+2)/2) * W / nf * 0.29'"
ps="'2*int((nf+2)/2) * (W / nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8_lvt


L_lvs=l
W_lvs="'w * nf '"

lvs_format="M@name @pinlist sky130_fd_pr__@model L=@L_lvs W=@W_lvs nf=@nf ad=@ad as=@as pd=@pd ps=@ps nrd=@nrd nrs=@nrs sa=@sa sb=@sb sd=@sd mult=@mult m=@mult"

}
