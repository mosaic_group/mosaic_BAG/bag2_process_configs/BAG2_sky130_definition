v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -70 -60 0 -60 {
lab=PLUS}
N 0 30 0 60 {
lab=MINUS}
N -70 60 0 60 {
lab=MINUS}
N 0 -60 0 -30 {
lab=PLUS}
C {devices/iopin.sym} -70 -60 0 1 {name=p3 lab=PLUS}
C {devices/iopin.sym} -70 60 0 1 {name=p1 lab=MINUS}
C {sky130_fd_pr/res_generic_m2.sym} 0 0 0 0 {name=R1
W=w
L=l
mult=1
model=res_generic_m2
spiceprefix=M}
