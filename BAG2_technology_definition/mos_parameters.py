"""
=========================
BAG2 Mos parameter module 
=========================

Collection of MOS transistor related technology parameters for 
BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 08.04.2022.

The goal of the class is to provide a master source for the MOS parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.mos_parameters_template import mos_parameters_template
from BAG2_methods.tech.si import µm
from .layer_parameters import si_conv


class mos_parameters(mos_parameters_template):

    @property
    def mos_pitch(self) -> int:
        """
        OD quantization pitch.
        """
        return 1

    @property
    def analog_unit_fg(self) -> int:
        """
        Number of fingers in an AnalogBase row must be multiples of this number.
        """
        return 2

    @property
    def draw_zero_extension(self) -> bool:
        """
        True if zero height AnalogMosExt should be drawn.
        """
        return False

    @property
    def floating_dummy(self) -> bool:
        """
        True if floating dummies are allowed.
        """
        return False

    @property
    def abut_analog_mos(self) -> bool:
        """
        True if AnalogMosConn can abut each other.
        """
        return True

    @property
    def draw_sub_od(self) -> bool:
        """
        True to draw OD in substrate contact.  False generally used to draw SOI transistors.
        """
        return True #DRM: tap and diff, npsdm.8 (Made changes in code to use 'tap' layer instead of 'diff' layer in sky130 technology, while drawing connections from substrate to other layers) 

    #    @property
    def sub_ring_lch(self) -> float:
        """
        channel length used in substrate rings.
        """
        return 150.0e-9

    @property
    def dum_conn_pitch(self) -> int:
        """
        vertical dummy wires pitch, in routing grid tracks.
        """
        return 1

    @property
    def dum_layer(self) -> int:
        """
        dummy connection layer.
        """
        return 2

    @property
    def ana_conn_layer(self) -> int:
        """
        AnalogBase vertical connection layer.
        """
        return 3

    @property
    def dig_conn_layer(self) -> int:
        """
        LaygoBase vertical connection layer
        """
        return 1

    @property
    def dig_top_layer(self) -> int:
        """
        LaygoBase top layer.
        """
        return 4

    @property
    def imp_od_encx(self) -> int:
        """
        horizontal enclosure of implant layers over OD
        """
        si_value = 0.065 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def od_spy_max(self) -> int:
        """
        maximum space between OD rows in resolution units.
        """
        return 1000000  # no dummy fill

    @property
    def od_min_density(self) -> float:
        """
        minimum OD density
        """
        return 0.0

    @property
    def od_fill_w(self) -> Tuple[int, int]:
        """
        dummy OD height range.
        """
        si_tuple = 0, 50 * µm

        unit_tuple = si_conv.convert_si_to_unit(si_tuple)
        return unit_tuple

    @property
    def md_w(self) -> int:
        """
        width of bottom bound-box of OD-METAL1 via
        Defines also x width of the quard ring
        """
        si_value = 0.17 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def od_spy(self) -> int:
        """
        minimum vertical space between OD, in resolution units
        """

        si_value = 0.34 * µm * 2
        # difftap.9: Spacing of (n+)-diff to N-well >= 0.34 µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def imp_od_ency(self) -> int:
        """
        implant layers vertical enclosure of active.
        this is used to figure out where to separate implant layers in extension blocks,
        """
        si_value = 0.13 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value
    
    @property
    def imp_tap_ency(self) -> int:
        """
        implant layers vertical enclosure of tap. #DRM:difftap.10
        this is used to figure out where to separate implant layers in extension blocks,
        """
        si_value = 0.18 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def imp_po_ency(self) -> int:
        """
        implant layers vertical enclosure of poly.
        this is used to figure out where to separate implant layers in extension blocks,
        if None, this rule is ignored.
        Does not seem to affect anything
        """
        return None

    @property
    def nw_dnw_ovl(self) -> int:
        """
        overlap between N-well layer and Deep N-well layer.
        """
        si_value = 1 * µm  # TODO: ?

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def nw_dnw_ext(self) -> int:
        """
        extension of N-well layer over Deep N-well layer.
        """
        si_value = 1 * µm  # TODO: ?

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def min_fg_decap(self) -> Dict[str, List[Union[float, int]]]:
        """
        Minimum number of fingers for decap connection.
        """
        return {
            'lch': [float('inf')],
            'val': [2]
        }

    @property
    def min_fg_sep(self) -> Dict[str, List[Union[float, int]]]:
        """
        Minimum number of fingers between separate AnalogMosConn.
        """
        return {
            'lch': [float('inf')],
            'val': [2]
        }

    @property
    def edge_margin(self) -> Dict[str, List[Union[float, int]]]:
        """
        space between AnalogBase implant and boundary.
        """

        si_dict = {
            'lch': [float('inf')],
            'val': [1.27 * µm] # DRM: nwell.2a (useful in hierarchical generators to space sub-blocks)
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def fg_gr_min(self) -> Dict[str, List[Union[float, int]]]:
        """
        minimum number of fingers needed for left/right guard ring.
        """
        return {
            'lch': [10, float('inf')],
            'val': [2, 1]
        }

    @property
    def fg_outer_min(self) -> Dict[str, List[Union[float, int]]]:
        """
        minimum number of fingers in outer edge block.
        """
        return {
            'lch': [float('inf')],
            'val': [2]
        }

    @property
    def sd_pitch_constants(self) -> Dict[str, List[Union[float, int, List[int]]]]:
        """
        source/drain pitch related constants.
        source/drain pitch is computed as val[0] + val[1] * lch_unit
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [[0.35 * µm, 2]] #DRM: poly.4 (helps to maintain a horizontal distance of 0.075um between diff and poly{last second from left and right side})
                                    #     For lch_unit < 2 we would trigger licon.5 drc error         
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def num_sd_per_track(self) -> Dict[str, List[Union[float, int]]]:
        """
        number of source/drain junction per vertical track.
        """
        return {
            'lch': [float('inf')],
            'val': [1]
        }

    @property
    def po_spy(self) -> Dict[str, List[Union[float, int]]]:
        """
        space between PO
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [0.27 * µm]
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def mx_gd_spy(self) -> Dict[str, List[Union[float, int]]]:
        """
        vertical space between gate/drain metal wires.
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [0.14 * µm]  # M1, M2 space (140nm)
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def od_gd_spy(self) -> Dict[str, List[Union[float, int]]]:
        """
        space between gate wire and OD
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [0.8 * µm]   # TODO: ?
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def po_od_exty(self) -> Dict[str, List[Union[float, int]]]:
        """
        PO extension over OD
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [0.13 * µm]   # on side without contact, 130nm
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def po_od_extx_constants(self) -> Dict[str, List[Union[float, int, Tuple[int, int, int]]]]:
        """
        OD horizontal extension over PO.
        value specified as a (offset, lch_scale, sd_pitch_scale) tuple,
        where the extension is computed as
        offset + lch_scale * lch_unit + sd_pitch_scale * sd_pitch_unit

        DEPRECATED: specify a constant number directly.
        (What does this mean)
        """
        return {
            'lch': [float('inf')],
            'val': [(-15, -1, 1)]  # DRM: poly.4(sets horizontal distance of 0.075um between last second poly from left and right side), 
                                   #      poly.8 and poly.10
        }

    @property
    def po_h_min(self) -> Dict[str, List[Union[float, int]]]:
        """
        minimum PO height
        """

        # TODO: @Matthias: please review
        #     the original code had a mismatch: 77 VS 355nm
        #     77 would be 0.385 * µm
        #     71 would b e 0.355 * µm
        # -------------------------------------
        # return {
        #     'lch': [float('inf')],
        #     'val': [77]  # 355nm
        # }

        si_dict = {
            'lch': [float('inf')],
            'val': [0.385 * µm]   # TODO: please review
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def sub_m1_extx(self) -> Dict[str, List[Union[float, int]]]:
        """
        distance between substrate METAL1 left edge to
        center of left-most source-drain junction.
        """

        si_dict = {
            'lch': [float('inf')],
            'val': [0.17 * µm]  # TODO: ?
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def sub_m1_enc_le(self) -> Dict[str, List[Union[float, int]]]:
        """
        substrate METAL1 via line-end enclosure
        """
        return {
            'lch': [float('inf')],
            'val': [0]  # li vertical extension on substrate contacts
        }

    @property
    def dum_m1_encx(self) -> Dict[str, List[Union[float, int]]]:
        """
        dummy METAL1 horizontal enclosure.
        """
        return {
            'lch': [float('inf')],
            'val': [0]  # li enclosure of dummy transistor wiring
        }

    @property
    def g_bot_layer(self) -> int:
        """
        gate wire bottom layer ID
        """
        return 1

    @property
    def d_bot_layer(self) -> int:
        """
        drain/source wire bottom layer ID
        """
        return 1

    @property
    def g_conn_w(self) -> Dict[str, List[Union[float, List[int]]]]:
        """
        gate vertical wire width on each layer.
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [[0.17 * µm,   0.28 * µm,     0.28 * µm]]
            #               li,   li+met1+met2,  met1+met2 (not sure what this means)
            # VS: these values have effect on horizontal track widths of [li, met1, met2] (also on some via parameters)
        }
        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def d_conn_w(self) -> Dict[str, List[Union[float, List[int]]]]:
        """
        drain vertical wire width on each layer.
        """
        si_dict = {
            'lch': [float('inf')],
            'val': [[0.17 * µm,   0.28 * µm,     0.32 * µm]]
            #               li,   li+met1+met2,  met1+met2 (not sure what this means)
            # VS: these values have effect on vertical track widths of [li, met1, met2] (also on some via parameters)
            # DRM: [          ,            ,          m2.5]]
        }
        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def g_conn_dir(self) -> List[str]:
        """
        gate wire directions
        """
        return ['x', 'y', 'y']

    @property
    def d_conn_dir(self) -> List[str]:
        """
        drain wire directions
        """
        return ['y', 'y', 'y']

    @property
    def v0_po_encx_min(self):
        """ horizontal enclosure of poly over gate contact via

        TODO: this parameter is not documented in the template
        """
        si_value = 0.05 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def g_via(self) -> Dict[str, List[int]]:
        """
        gate via parameters
        --------------------------------------

        Returns dictionary
        with entries (index is layer id):
           'dim': list of (w, h) dimension tuples (per layer)
           'sp':  list of spaces (per layer)
           'bot_enc_le': list of bottom enclosure length (per layer)
           'top_enc_le': list of top enclosure length (per layer)
        """


        si_dict = {
            'dim': [
                (0.17 * µm, 0.17 * µm),   # licon w, h (170nm x 170nm)
                (0.17 * µm, 0.17 * µm),   # mcon w, h (170nm x 170nm)
                (0.15 * µm, 0.15 * µm)],  # v1 w, h (170nm x 170nm)   TODO: please review

            # DRM: [licon.2,       ct.2,     via.2]
            'sp': [0.17 * µm,   0.19 * µm, 0.17 * µm],  # vertical spacing for licon, mcon, v1

            # DRM:        [         ,  m1.4a/m1.4, via.4a/via(1).5a]
            'bot_enc_le': [0.08 * µm,  0.03 * µm,  0.085 * µm],  # licon/po(y), mcon/m1(x), v1/m1(y) enclosure line end
            'top_enc_le': [0.08 * µm,  0.08 * µm,  0.055 * µm]  # licon/li(x), mcon/li(y), v1/m2(y) enclosure line end
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def prim_offset(self) -> int:
        """
        Y direction offset for the primitive used in Analog base

        Currently used at Aalto
        """
        raise NotImplementedError("Only used at Aalto")

    @property
    def d_via(self) -> Dict[str, List[int]]:
        """
        drain/source via parameters
        """
        return self.g_via

    @property
    def substrate_contact(self) -> Dict[str, List[int]]:
        """
        Substrate contact parameter
        Copy d_via here if needed. Used Analog Base if
        substrate contacs differ from drain contacts
        """
        raise NotImplementedError("substrate_contact() is not implemented in this BAG2_technology_definition!")

    @property
    def dnw_layers(self) -> List[Tuple[str, str]]:
        """
        Deep N-well layer names
        """
        return [
            ('dnwell', 'drawing'),
        ]

    @property
    def imp_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """
        implant layer names for each transistor/substrate tap type.
        """
        return {
            # TODO: what is the purpose of the value array (I don't see an effect :( )
            # 'nch_thick' : {
            #     ('layer', 'purpose') : [0,0],
            #     ('layer', 'purpose') : [0,0]
            # },
            'nch': {
                ('nsdm', 'drawing'): [0, 0],
            },
            'pch': {
                ('psdm', 'drawing'): [0, 0],
                ('nwell', 'drawing'): [0, 0],
            },
            'ptap': {
                ('psdm', 'drawing'): [0, 0]
                #('tap', 'drawing'): [0, 0] #Don't include tap layer as it is already used by code for creating tap connections from substrates 
            },
            'ntap': {
                ('nsdm', 'drawing'): [0, 0],
                ('nwell', 'drawing'): [0, 0]
                #('tap', 'drawing'): [0, 0] #Don't include tap layer as it is already used by code for creating tap connections from substrates
            }
        } ## MOSTechPlanarGeneric class in BAG2_technology_definition knows how to draw a tap via if sub_type is 'ptap' or 'ntap'

    @property
    def thres_layers(self) -> Dict[str, Dict[str, Dict[Tuple[str, str], List[int]]]]:
        """
        threshold layer names for each transistor/substrate tap type.
        """
        return {
            'nch': {
                'standard': {
                },
                'lvt': {
                    ('lvtn', 'drawing'): [0, 0]  # @@@
                },
                'hvt': {
                    ('hvtp', 'drawing'): [0, 0]  # @@@
                },
            },
            'pch': {
                'standard': {
                },
                'lvt': {
                    ('lvtn', 'drawing'): [0, 0]  # @@@
                },
                'hvt': {
                    ('hvtp', 'drawing'): [0, 0]  # @@@
                },
            },
            'ptap': {
                'standard': {
                },
                'lvt': {
                },
                'hvt': {
                },
            },
            'ntap': {
                'standard': {
                },
                'lvt': {
                },
                'hvt': {
                },
            }
        }

    @property
    def fin_h(self) -> int:
        """
        fin height (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def od_spx(self) -> int:
        """
        minimum horizontal space between OD, in resolution units (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def od_fin_exty_constants(self) -> List[int]:
        """
        Optional: OD vertical extension over fins (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def od_fin_extx(self) -> int:
        """
        Horizontal enclosure of fins over OD (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def no_sub_dummy(self) -> bool:
        """
        True if dummies cannot be drawn on substrate region (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mos_conn_modulus(self) -> int:
        """
        Source/drain modulus for transistor/dummy connections.


        Allows transistor/dummy connections to change their geometry based
        on the source column index modulo by this number.
        """
        return 1

    @property
    def od_fill_h(self) -> List[int]:
        """
        Dummy OD height range (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def od_fill_w_max(self) -> Optional[int]:
        """
        Dummy OD maximum width, in resolution units (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def imp_min_w(self) -> int:
        """
        Minimum implant layer width (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def imp_edge_dx(self) -> Dict[Tuple[str, str], List[int]]:
        """
        Dictionary from implant layers to X-delta in outer edge blocks (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_h_sub(self) -> int:
        """
        Substrate MP height (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_spy_sub(self) -> int:
        """
        Substrate MP vertical space (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_po_ovl_constants_sub(self) -> List[int]:
        """
        Substrate MP extension/overlap over PO (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_md_sp_sub(self) -> int:
        """
        Substrate MP space to MD (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_cpo_sp_sub(self) -> int:
        """
        Substrate MP space to CPO (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_h(self) -> int:
        """
        MP height (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_spy(self) -> int:
        """
        Vertical space between MP (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_po_ovl_constants(self) -> List[int]:
        """
        MP and PO overlap (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_md_sp(self) -> int:
        """
        Space bewteen MP and MD (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def mp_cpo_sp(self) -> int:
        """
        Space between MP and CPO (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def has_cpo(self) -> bool:
        """
        True to draw CPO (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def cpo_h(self) -> Dict[str, List[Union[float, int]]]:
        """
        Normal CPO height (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def cpo_po_extx(self) -> Dict[str, List[Union[float, int]]]:
        """
        Horizontal extension of CPO beyond PO (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def cpo_po_ency(self) -> Dict[str, List[Union[float, int]]]:
        """
        Vertical enclosure of CPO on PO (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def cpo_od_sp(self) -> Dict[str, List[Union[float, int]]]:
        """
        CPO to OD spacing (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def cpo_spy(self) -> Dict[str, List[Union[float, int]]]:
        """
        CPO to CPO vertical spacing (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def cpo_h_end(self) -> Dict[str, List[Union[float, int]]]:
        """
        CPO height for substrate end (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def md_od_exty(self) -> Dict[str, List[Union[float, int]]]:
        """
        Vertical extension of MD over OD (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def md_spy(self) -> Dict[str, List[Union[float, int]]]:
        """
        Vertical space bewteen MD (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def md_h_min(self) -> Dict[str, List[Union[float, int]]]:
        """
        Minimum height of MD (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def dpo_edge_spy(self) -> int:
        """
        Vertical space between gate PO when no CPO is used for dummy transistors (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def g_m1_dum_h(self) -> int:
        """
        Gate M1 dummy wire height (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def ds_m2_sp(self) -> int:
        """
        Drain/source M2 space (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def property_dict(self) -> Dict:
        """
        Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.
        """
        if not hasattr(self, '_property_dict'):
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip parameters (i.e. FINFET params for a planar tech)
        return self._property_dict
