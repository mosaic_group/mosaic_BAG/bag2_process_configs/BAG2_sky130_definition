"""
======
BAG2 resistor parameter module
======

The resistor parameter module of BAG2 framework.

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 12.4.2022.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.resistor_parameters_template import resistor_parameters_template
from BAG2_methods.tech.si import µm
from .layer_parameters import si_conv


class resistor_parameters(resistor_parameters_template):

    @property
    def bot_layer(self) -> int:
        """
        Bottom horizontal routing layer ID
        """
        return 1

    @property
    def block_pitch(self) -> Tuple[int, int]:
        """
        Resistor core block pitch in resolution units
        """
        return 1, 1

    @property
    def po_sp(self) -> int:
        """
        Space between PO and dummy PO
        """
        si_value = 0.7 * µm      # TODO: ?

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def imp_od_sp(self) -> int:
        """
        Space between implant layer and OD.
        Used only if OD cannot be inside resistor implant.
        """
        return 0

    @property
    def po_od_sp(self) -> int:
        """ space between PO/dummy PO and dummy OD """
        si_value = 0.27 * µm      # DRM: difftap.3, licon.14 ## This rule limits the drawing of OD blocks in top and bottom of rescore cell

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def po_co_enc(self) -> Tuple[int, int]:
        """  PO horizontal/vertical enclosure of CONTACT """

        si_tuple = (0.05 * µm, 0.05 * µm)

        unit_tuple = si_conv.convert_si_to_unit(si_tuple)
        return unit_tuple

    @property
    def po_rpo_ext_exact(self) -> int:
        """ exact extension of PO over RPO.  If negative, this parameter is ignored. """
        return -1

    @property
    def po_max_density(self) -> float:
        """ maximum PO density (recommended) """
        return 0.6

    @property
    def dpo_dim_min(self) -> Tuple[int, int]:
        """ dummy PO minimum width/height """

        # TODO: @Matthias: please review
        #     the original code had a mismatch: 30 VS 170nm
        #     30 would be 0.15 * µm
        #     34 would be 0.17 * µm
        # -------------------------------------
        # return [75, 75]  # 150nm

        si_tuple = (0.375 * µm, 0.375 * µm)   #   TODO: please review

        unit_tuple = si_conv.convert_si_to_unit(si_tuple)
        return unit_tuple

    @property
    def od_dim_min(self) -> Tuple[int, int]:
        """  dummy OD minimum width/height """
        si_tuple = (0.15 * µm, 0.15 * µm)

        unit_tuple = si_conv.convert_si_to_unit(si_tuple)
        return unit_tuple

    @property
    def od_dim_max(self) -> Tuple[int, int]:
        """  dummy OD maximum width/height """
        si_tuple = (3 * µm, 3 * µm)

        unit_tuple = si_conv.convert_si_to_unit(si_tuple)
        return unit_tuple

    @property
    def od_sp(self) -> int:
        """  dummy OD space """
        si_value = 0.27 * µm #DRM: difftap.3

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value


    @property
    def od_min_density(self) -> float:
        """  minimum OD density """
        return 0.2

    @property
    def co_w(self) -> int:
        """  CONTACT width """
        si_value = 0.17 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value
    
    @property
    def co_h(self) -> int:
            """  CONTACT height """
            si_value = 0.17 * µm

            unit_value = si_conv.convert_si_to_unit(si_value)
            return unit_value

    @property
    def co_sp(self) -> int:
        """  CONTACT spacing """
        si_value = 0.17 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def m1_co_enc(self) -> Tuple[int, int]:
        """  METAL1 horizontal/vertical enclosure of CONTACT """
        si_value = 0.08 * µm, 0.08 * µm # DRM: li.5

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value


    @property
    def m1_sp_max(self) -> int:
        """  METAL1 fill maximum spacing """
        si_value = 5 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def m1_sp_bnd(self) -> int:
        """  METAL1 fill space to boundary """
        si_value = 0.5 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def rpo_co_sp(self) -> int:
        """  space of RPO to CONTACT """
        si_value = 0.2 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def rpo_extx(self) -> int:
        """  extension of RPO on PO """
        si_value = 0.5 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def edge_margin(self) -> int:
        """  margin needed on the edges """
        si_value = 0.5 * µm

        unit_value = si_conv.convert_si_to_unit(si_value)
        return unit_value

    @property
    def imp_enc(self) -> Tuple[int, int]:
        """  enclosure of implant layers in horizontal/vertical direction """
        si_tuple = (0.25 * µm, 0.25 * µm)

        unit_tuple = si_conv.convert_si_to_unit(si_tuple)
        return unit_tuple

    @property
    def imp_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """ resistor implant layers list """
        return {
            'nch': {
                ('nsdm', 'drawing'): [100, 300],  # TODO: what are the values?
            },
            'pch': {
                ('psdm', 'drawing'): [100, 300],  # TODO: what are the values?
                ('nwell', 'drawing'): [100, 300],
            },
            'ptap': {
                ('psdm', 'drawing'): [100, 300],  # TODO: what are the values?
            },
            'ntap': {
                ('psdm', 'drawing'): [100, 300],  # TODO: what are the values?
                ('nwell', 'drawing'): [100, 300],
            }
        }

    @property
    def res_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """  resistor layers list """
        return {
            # TODO: implement
            'standard': {
            },
            'high_speed': {
            }
        }

    @property
    def thres_layers(self) -> Dict[str, Dict[str, Dict[Tuple[str, str], List[int]]]]:
        # TODO: implement
        return {
            'ptap': {
                'standard': {},
                'svt': {},
                'lvt': {
                    ('lvtn', 'drawing'): [0, 0],
                }
            },
            'ntap': {
                'standard': {},
                'svt': {},
                'lvt': {
                    ('lvtp', 'drawing'): [0, 0],
                }
            },
        }

    @property
    def info(self) -> Dict:
        """  resistor type information dictionary """
        # TODO: implement
        return {
            'standard': {
                'rsq': 500,
                'min_nsq': 1,
                'w_bounds': (0.4, 2.0),
                'l_bounds': (0.4, 25.0),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo': False,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy': True,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res': True,
            },
            'high_speed': {
                'rsq': 500,
                'min_nsq': 0.2,
                'w_bounds': (0.4, 2.0),
                'l_bounds': (0.4, 25.0),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo': False,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy': True,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res': True,
            },
        }

    @property
    def property_dict(self) -> Dict:
        """
        Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.
        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            # self._process_config = {}
            #   dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary['mos']
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip parameters (i.e. FINFET params for a planar tech)

        return self._property_dict
