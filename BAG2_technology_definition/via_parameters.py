"""
=========================
BAG2 Via parameter module 
=========================

Collection of Via related technology para,eters for 
BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 11.04.2022.

The goal of the class is to provide a master source for the Via parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.via_parameters_template import via_parameters_template
from BAG2_methods.tech.si import µm
from .layer_parameters import si_conv


class via_parameters(via_parameters_template):

    @property
    def via_units(self) -> Dict:
        """
        Property that returns a dict of via units of instances of class 'via_unit'
        NOTE: see BAG2_framework/bag/layout/core.py get_via_drc_info() return types
        """

        if hasattr(self, '_via_units'):
            return self._via_units

        via_L1M1_C_square_si_dict = {
            # Dimensions
            'dim': (0.17 * µm, 0.17 * µm),
            # via horizontal/vertical spacing
            'sp': (0.19 * µm, 0.19 * µm),
            # list of valid via horizontal/vertical spacings when it's a 2x2 array
            # 'sp2': List[Tuple[int, int]],
            # list of valid via horizontal/vertical spacings when it's a mxn array,
            # where min(m, n) >= 2, max(m, n) >= 3
            # 'sp3' : List[Tuple[int, int]],
            # via bottom enclosure rules.  If empty, then assume it's the same
            # as top enclosure.
            'bot_enc': None,
            # via top enclosure rules.
            'top_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.06 * µm, 0.06 * µm)]
                ]
            }
        }

        via_L1M1_C_square_units_dict = si_conv.convert_si_to_unit(via_L1M1_C_square_si_dict)

        via_M1M2_C_square_si_dict = {
            # Dimensions
            'dim': (0.15 * µm, 0.15 * µm),
            # via horizontal/vertical spacing
            'sp': (0.17 * µm, 0.17 * µm),
            # list of valid via horizontal/vertical spacings when it's a 2x2 array
            # 'sp2': List[Tuple[int, int]],
            # list of valid via horizontal/vertical spacings when it's a mxn array,
            # where min(m, n) >= 2, max(m, n) >= 3
            # 'sp3' : List[Tuple[int, int]],
            # via bottom enclosure rules.  If empty, then assume it's the same
            # as top enclosure.
            'bot_enc': None,
            # via top enclosure rules.
            'top_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.085 * µm, 0.055 * µm),
                     (0.055 * µm, 0.085 * µm)]
                ]
            }
        }

        via_M1M2_C_square_units_dict = si_conv.convert_si_to_unit(via_M1M2_C_square_si_dict)

        via_M2M3_C_square_si_dict = {
            # Dimensions
            'dim': (0.2 * µm, 0.2 * µm),
            # via horizontal/vertical spacing
            'sp': (0.2 * µm, 0.2 * µm),
            # list of valid via horizontal/vertical spacings when it's a 2x2 array
            # 'sp2': List[Tuple[int, int]],
            # list of valid via horizontal/vertical spacings when it's a mxn array,
            # where min(m, n) >= 2, max(m, n) >= 3
            # 'sp3' : List[Tuple[int, int]],
            # via bottom enclosure rules.  If empty, then assume it's the same
            # as top enclosure.
            'bot_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.085 * µm, 0.04 * µm),
                     (0.04 * µm, 0.085 * µm)]
                ]
                # Each tuple is a pair of (extension, enclosure) values. Depending on the direction of metal layer used in via
                # index of enclosure and extension values within this tuple are chosen. BAG chooses that tuple that results in minimum extension if parameter 'extend'=False
                # For example, if bottom metal layer is having a direction of 'y' then zeroth idx of tuple -> enclosure values and first idx of tuple is the extension value.
                # These values must bounded by the width and height of the bounding box(BBox) of the via. Width and height of BBox are usually the widths of top and bottom layer
                # based on their respective directions 
            },
            # via top enclosure rules.
            'top_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.085 * µm, 0.065 * µm),
                     (0.065 * µm, 0.085 * µm)]
                ]
            }
        }

        via_M2M3_C_square_units_dict = si_conv.convert_si_to_unit(via_M2M3_C_square_si_dict)

        via_M3M4_C_square_si_dict = {
            # Dimensions
            'dim': (0.2 * µm, 0.2 * µm),
            # via horizontal/vertical spacing
            'sp': (0.2 * µm, 0.2 * µm),
            # list of valid via horizontal/vertical spacings when it's a 2x2 array
            # 'sp2': List[Tuple[int, int]],
            # list of valid via horizontal/vertical spacings when it's a mxn array,
            # where min(m, n) >= 2, max(m, n) >= 3
            # 'sp3' : List[Tuple[int, int]],
            # via bottom enclosure rules.  If empty, then assume it's the same
            # as top enclosure.
            'bot_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.09 * µm, 0.055 * µm),
                     (0.055 * µm, 0.09 * µm)]
                ]
            },
            # via top enclosure rules.
            'top_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.065 * µm, 0.065 * µm)]
                ]
            }
        }

        via_M3M4_C_square_units_dict = si_conv.convert_si_to_unit(via_M3M4_C_square_si_dict)

        via_M4M5_C_square_si_dict = {
            # Dimensions
            'dim': (0.8 * µm, 0.8 * µm),
            # via horizontal/vertical spacing
            'sp': (0.8 * µm, 0.8 * µm),
            # list of valid via horizontal/vertical spacings when it's a 2x2 array
            # 'sp2': List[Tuple[int, int]],
            # list of valid via horizontal/vertical spacings when it's a mxn array,
            # where min(m, n) >= 2, max(m, n) >= 3
            # 'sp3' : List[Tuple[int, int]],
            # via bottom enclosure rules.  If empty, then assume it's the same
            # as top enclosure.
            'bot_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.19 * µm, 0.19 * µm)]
                ]
            },
            # via top enclosure rules.
            'top_enc': {
                # via enclosure rule as function of wire width.
                'w_list': [float('Inf')],
                # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                # is used if wire width is less than or equal to w_list[idx].
                'enc_list': [
                    [(0.31 * µm, 0.31 * µm)]
                ]
            }
        }

        via_M4M5_C_square_units_dict = si_conv.convert_si_to_unit(via_M4M5_C_square_si_dict)

        self._via_units = {}

        # Next unit
        name = 'L1M1_C'
        self._via_units[name] = via_unit(name=name)
        self._via_units[name].square = via_L1M1_C_square_units_dict

        # Next unit
        name = 'M1M2_C'
        self._via_units[name] = via_unit(name=name)
        self._via_units[name].square = via_M1M2_C_square_units_dict

        name = 'M2M3_C'
        # Next unit
        self._via_units[name] = via_unit(name=name)
        self._via_units[name].square = via_M2M3_C_square_units_dict

        name = 'M3M4_C'
        # Next unit
        self._via_units[name] = via_unit(name=name)
        self._via_units[name].square = via_M3M4_C_square_units_dict

        name = 'M4M5_C'
        # Next unit
        self._via_units[name] = via_unit(name=name)
        self._via_units[name].square = via_M4M5_C_square_units_dict

        return self._via_units

    @property
    def via_name(self) -> Dict[int, str]:
        """
        Dictionary of via types between layer types.
        """
        return {
            1: 'L1M1_C',  # mcon between li1 and met1
            2: 'M1M2_C',  # via between met1 and met2
            3: 'M2M3_C',  # via2 between met2 and met3
            4: 'M3M4_C',  # via3 between met3 and met4
            5: 'M4M5_C',  # via4 between met4 and met5
        }

    @property
    def via_id(self) -> Dict[Tuple[str, str], str]:
        """
        Dictionary for mapping a via to connect of two layers to a
        via primitive of the vendor.
        NOTE: this uses the definitions from opensource.yaml.
        """
        return {
            (('diff', 'drawing'), 'li1'): 'DIL1_C', #Separating vias for diff to li connection
            (('tap', 'drawing'), 'li1'): 'TPL1_C',  # and tap to li connection from substrate
            (('poly', 'drawing'), 'li1'): 'PYL1_C',
            ('li1', 'met1'): 'L1M1_C',
            ('met1', 'met2'): 'M1M2_C',
            ('met2', 'met3'): 'M2M3_C',
            ('met3', 'met4'): 'M3M4_C',
            ('met4', 'met5'): 'M4M5_C',
        }

    @property
    def property_dict(self) -> Dict:
        """
        Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.
        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            #    self._process_config = {}
            #    dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary['via']
            self._property_dict = {}
            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self, key)

            for key, val in self.via_units.items():
                self._property_dict[key] = val.property_dict
        return self._property_dict


class via_unit:
    def __init__(self, **kwargs):
        if kwargs.get('name'):
            self._name = kwargs.get('name')
        else:
            print('Must give name for a via instance')

    @property
    def name(self):
        if not hasattr(self, '_name'):
            self._name = None
        return self._name

    @property
    def square(self):
        if not hasattr(self, '_square'):
            self._square = None
        return self._square

    @square.setter
    def square(self, val):
        self._square = val

    @property
    def hrect(self):
        if not hasattr(self, '_hrect'):
            self._hrect = self._square
        return self._hrect

    @hrect.setter
    def hrect(self, val):
        self._hrect = val

    @property
    def property_dict(self):
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """

        if not hasattr(self, '_property_dict'):
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip parameters (i.e. FINFET params for a planar tech)

        return self._property_dict
