"""
======
BAG2 layer definition module
======

The layer definition module of BAG2 framework. Currently collects all definition not
directly related to 

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 2020.

The goal of the class is to provide a master source for the process information. 
Currently it inherits TechInfoConfig, which in turn uses the values defined in it.
This circularity should be eventually broken.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.layer_parameters_template import layer_parameters_template
from BAG2_methods.tech.si import µm, SIConverter

si_conv = SIConverter(layout_unit_in_meters=1e-6,
                      resolution_in_layout_units=0.005)

# NOTE:
#    there is no DRM PDF part of the open source sky130A PDK,
#    but this webpage contains tables with relevant design rules:
#    https://skywater-pdk.readthedocs.io/en/main/rules/assumptions.html#minimum-critical-dimensions
#
#    another useful source:
#    https://github.com/ucb-art/skywater-pdk-libs-sky130_bag3_pr/blob/main/src/templates_skywater130/data/
#
#    Metal Stack:
#    https://skywater-pdk.readthedocs.io/en/main/_images/metal_stack.svg


class layer_parameters(layer_parameters_template):

    @property
    def tech_lib(self) -> str:
        """
        Name of the process technology library containing transistor primitives.
        """
        return 'skywater130'

    @property
    def layout_unit(self) -> float:
        """
        Layout unit, in meters
        """
        return 1.0e-6

    @property
    def resolution(self) -> float:
        """
        Layout resolution in layout units.
        """
        return 0.005

    @property
    def flip_parity(self) -> bool:
        """
        True if this is multipatterning technology.
        """
        return False

    @property
    def pin_purpose(self) -> str:
        """
        Purpose of the pin layer used in pins constructs
            
              str : Default 'pin'
        """
        return 'pin'

    @property
    def well_layers(self) -> Dict[str, List[Tuple[str, str]]]:
        """Dictionary of tuples of form
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys
        ------------
                ntap : (str,str)
                ptap : (str,str)

        """
        return {
            'ntap': ('nwell', 'drawing'),
            'ptap': []
        }

    @property
    def mos_layer_table(self) -> Dict[str, Tuple[str, str]]:
        """Dictionary of tuples of form
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys
        ------------
                PO : (str,str), Poly layer
                PO_sub : (str,str), Substrate Poly layer
                PO__gate_dummy : (str,str), Dummy Poly on dummy OD
                PO_dummy : (str,str), Dummy Poly not on any OD
                PO_edge : (str,str), Edge Poly
                PO_edge_sub : (str,str), Edge Poly on substrate OD
                PO_edge_dummy : (str,str), Edge Poly on dummy OD
                PODE : None, Poly on OD edge layer

                OD : (str,str), Active layer
                OD_sub: (str,str), Substrate active layer
                OD_dummy: (str,str), Dummy active layer

                MP: (str,str), Gate connection metal
                MD: (str,str), OD connection metal
                MD_dummy: (str,str), dummy OD connection metal

                CPO: (str,str), cut poly
                FB: (str, str), fin boundary layer

        """
        return {
            'PO': ('poly', 'drawing'),
            'PO_dummy': ('poly', 'drawing'),
            # TODO: no dummy purpose provided currently - how to avoid dummies at all?
            'OD': ('diff', 'drawing'),
            'OD_dummy': ('diff', 'drawing'),
            'TAP': ('tap','drawing') #Added new 'TAP' layer to enable substrate connections to start from tap layer instead of diff layer.
            # TODO: no dummy purpose provided currently - how to avoid dummies at all?
        }

    @property
    def res_layer_table(self) -> Dict[str, Tuple[str, str]]:
        """Dictionary of tuples of form
        key:  (layer,purpose)
        describing the layers used for a metal resistors.

        Current keys
        ------------
                RPDMY : (str,str), a layer drawn exactly on top of metal resistor,
                        generally used for LVS recognition purposes.

                RPO : (str,str), the "resistive poly" layer that makes a poly more resistive.

        """
        # TODO: is that applying to poly resistors only? Description says "metal" ...
        return {
            'RPDMY': ('poly', 'res'),  # TODO: not clear what to use here for sky 130
            'RPO': ('poly', 'res'),  # TODO: translation to sky 130?
        }

    @property
    def res_metal_layer_table(self) -> Dict[int, List[Tuple[str, str]]]:
        """ Mapping from metal layer ID to layer/purpose pair that defines
        a metal resistor.

        Array of tuples of form (layer,purpose)
        INdex 0 is usually (None,None) as there ins no Metal0

        """
        return {
            1: [('li1', 'res')],
            2: [('met1', 'res')],
            3: [('met2', 'res')],
            4: [('met3', 'res')],
            5: [('met4', 'res')],
            6: [('met5', 'res')],
        }

    @property
    def metal_exclude_table(self) -> Dict[int, Tuple[str, str]]:
        """Mapping from metal layer ID to layer/purpose pair that
        defines metal exclusion region.

        Dict of tuples of form int : (layer,purpose)
        """
        # TODO: what is the meaning of these layers? And what layers to use in sky 130?
        return {
            1: ('li1', 'blockage'),
            2: ('met1', 'blockage'),
            3: ('met2', 'blockage'),
            4: ('met3', 'blockage'),
            5: ('met4', 'blockage'),
            6: ('met5', 'blockage'),
        }

    @property
    def layer_name(self) -> Dict[Union[int, str], str]:
        """
        Mapping dictionary from metal layer ID to layer name. Assume purpose is 'drawing'.

        Dict of of form int : 'layer'

        """
        return {
            1: 'li1',
            2: 'met1',
            3: 'met2',
            4: 'met3',
            5: 'met4',
            6: 'met5',
        }

    @property
    def layer_type(self) -> Dict[str, str]:
        """Mapping from metal layer name to metal layer type.  The layer type
         is used to figure out which EM rules to use. OD has no current density
         EM rules, but layer type is used for via mapping as well. Hence, it
         is also here.

        Dict of form int : 'layer'

        """
        return {
            'li1': 'li1',  # w=0.17
            'met1': 'met1',  # w=0.14
            'met2': 'met2',  # w=0.14
            'met3': 'met3',  # w=0.3
            'met4': 'met4',  # w=0.3
            'met5': 'met5',  # w=1.6
        }

    @property
    def max_w(self) -> Dict[str, int]:
        """
        Maximum wire width for given metal type
        """
        raise NotImplementedError("max_w() is not implemented in this BAG2_technology_definition!")

    @property
    def min_area(self) -> Dict[str, List[int]]:
        """
        Minimum area for given metal type
        """
        raise NotImplementedError("min_area() is not implemented in this BAG2_technology_definition!")

    @property
    def sp_le_min(self) -> Dict[str, Dict[str, List[Union[int, float]]]]:
        """
        Minimum line-end spacing rule. Space is measured parallel to wire direction

        ------
        Returns
        Dict Structure (NOTE: a,b,x,y,z ∈ int):
        {
            w_list: [a, b, inf]    # values in units
            sp_list: [x, y, z]   # values are in units
        }

        Illustration:
            widths(unit):   min_w       a         b        inf
            space(unit):            x         y       z

        NOTE: The min_w is implicit (not "part" of the w_list)

        Interpretation:
            - Space x:  valid for width interval [0, a[
            - Space y:  valid for width interval [a, b[
            - Space z:  valid for width interval [b, inf[
        """

        si_dict = {
            'li1': {
                'w_list': [float('Inf')],
                'sp_list': [0.17 * µm],
            },
            'met1': {
                'w_list': [float('Inf')],
                'sp_list': [0.155 * µm],
            },
            'met2': {
                'w_list': [float('Inf')],
                'sp_list': [0.155 * µm],
            },
            'met3': {
                'w_list': [float('Inf')],
                'sp_list': [0.3 * µm],
            },
            'met4': {
                'w_list': [float('Inf')],
                'sp_list': [0.3 * µm],
            },
            'met5': {
                'w_list': [float('Inf')],
                'sp_list': [1.6 * µm],
            },
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def len_min(self) -> Dict[str, Dict[str, Any]]:
        """
        Minimum length/minimum area rules.

        ------
        Returns
        Dict Structure:
        {
            w_list:    [a,   b,   inf] # values in units
            w_al_list: [(a1, l1), (a2, l2), (a3, l3)]    # area values are in square-units, length in units

            md_list:    [x, y, inf]
            md_al_list: [(a1, l1), (a2, l2), (a3, l3)]   # area values are in square-units, length in units
        }

        The pairs (`w_list`/`w_al_list` and `md_list`/`md_al_list`) belong together.

        ---

        The w-list is used to determine the minimum length for a given width.
        The index-correlated lists are searched through in forward direction,
        until the first w_list entry is found where given width fits.

        Illustration w-list:
            w_list(unit):     min_w        a            b            inf
            w_al_list(unit):      (a1, l1)    (a2, l2)     (a3, l3)
        NOTE: The min_w is implicit (not "part" of the w_list)

        Interpretation:
            - (a1, l1): valid area/length for width interval [min_w, a[
            - (a2, l2): valid area/length for width interval [a, b[
            - (a3, l3): valid for width interval [b, inf[

        ---

        The md-list is used to determine the maximum dimension for a given width.
        The index-correlated lists are searched through in reverse direction,
        until the first max_dim list entry is found where given width/length fits (is larger).
        """
        # These values aren't used until explicitly specified...for example the 'min_len_mode' argument of connect_to_tracks() method 
        # has to be set to an 'int' value for BAG to check for minimum and maximum area/length(not width) related rules.
        # Check get_min_length() method of TechInfoConfig class from ./BAG2_framework/bag/layout/tech.py for more info

        # w_al_list = [min. area for the metal layer, min. length ]  #DRM: m1.6, m2.6, m3.6, m4.4a, m5.4
        # Due to the logic present in get_min_length(), we delibrately set 'min. length' of 'w_al_list' as 0.
        # Doing this results in layout generated always satisfying only the defined minimum area criteria (unless explicitly using more area while manual routing).
        # Hence saving the total area of metal used. 

        si_dict = {
            'li1': {
                'w_list': [float('Inf')],
                #'w_al_list': [(0.0561 * µm**2, 0.33 * µm)],
                'w_al_list': [(0.0561 * µm**2, 0)],
                'md_list': [],
                'md_al_list': [],
            },
            'met1': {
                'w_list': [float('Inf')],
                #'w_al_list': [(0.083 * µm**2, 0.593 * µm)],
                'w_al_list': [(0.083 * µm**2, 0)],
                'md_list': [],
                'md_al_list': [],
            },
            'met2': {
                'w_list': [float('Inf')],
                #'w_al_list': [(0.0676 * µm**2, 0.483 * µm)],
                'w_al_list': [(0.0676 * µm**2, 0)],
                'md_list': [],
                'md_al_list': [],
            },
            'met3': {
                'w_list': [float('Inf')],
                #'w_al_list': [(0.24 * µm**2, 0.8 * µm)],
                'w_al_list': [(0.24 * µm**2, 0)],
                'md_list': [],
                'md_al_list': [],
            },
            'met4': {
                'w_list': [float('Inf')],
                #'w_al_list': [(0.24 * µm**2, 0.8 * µm)],
                'w_al_list': [(0.24 * µm**2, 0)],
                'md_list': [],
                'md_al_list': [],
            },
            'met5': {
                'w_list': [float('Inf')],
                #'w_al_list': [(4 * µm**2, 2.5 * µm)],
                'w_al_list': [(4 * µm**2, 0)],
                'md_list': [],
                'md_al_list': [],
            },
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def idc_em_scale(self) -> Dict[str, Any]:
        """Table of electromigration temperature scale factor
        """
        return {
            # scale factor for resistor
            # scale[idx] is used if temperature is less than or equal to temp[idx]
            'res': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
                # scale factor for this metal layer type
            },
            # TODO: is this per type?
            'li1': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            },
            'met1': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            },
            'met2': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            },
            'met3': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            },
            'met4': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            },
            'met5': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            },
            # default scale vector
            'default': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5]
            }
        }

    @property
    def sp_min(self) -> Dict[str, Dict[str, List[Union[float, int]]]]:
        """
        Minimum wire spacing rule.  Space is measured orthogonal to wire direction.
        Wire spacing as function of wire width.  sp_list[idx] is used if wire width is
        less than or equal to w_list[idx].
        """
        return self.sp_le_min

    @property
    def sp_sc_min(self) -> Dict[str, Dict[str, List[Union[float, int]]]]:
        """
        Minimum wire spacing rule for same color wires (FINFET specific parameter)
        """
        raise NotImplementedError("FINFET specific parameter, this is a planar tech")

    @property
    def dnw_margins(self) -> Dict[str, int]:
        """
        Deep Nwell margins
        """
        # TODO: review these values, whatever this means ...
        return {
            'normal': 250,
            'adjacent': 250,
            'compact': 250
        }

    #            'dnw_dnw_sp' :  int , # Deep N-well to Deep N-well with different potential space
    #            'dnw_nw_sp' :  int , # Deep N-well to N-well with different potential space
    #            'dnw_pw_sp' :  int ,
    #            'dnw_np_enc' :  int , # Minimum Deep N-well enclosure of NP
    #           }

    @property
    def implant_rules(self) -> Dict[str, Any]:
        """
        Implant layer rules.
        """
        raise NotImplementedError("implant_rules() is not implemented in this BAG2_technology_definition!")

    @property
    def nw_rules(self) -> Dict[str, int]:
        """
        N-well layer rules.
        """
        raise NotImplementedError("nw_rules() is not implemented in this BAG2_technology_definition!")

    @property
    def od_rules(self) -> Dict[str, Any]:
        """
        OD layer rules
        """
        raise NotImplementedError("od_rules() is not implemented in this BAG2_technology_definition!")

    @property
    def property_dict(self):
        """
        Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.
        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            #   #self._process_config = {}
            #    dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        if key == 'flip_parity':
                            self._property_dict['use_flip_parity'] = getattr(self, key)
                        else:
                            self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip parameters (i.e. FINFET params for a planar tech)
        return self._property_dict
