"""BAG2 technology definition
   --------------------------
Class to defne the technology for BAG2 framework

Initially written by Marko Kosunen, Aalto University, 2019.
"""
import copy
import importlib
import os
import pkg_resources
from typing import *
import yaml

from bag.layout.template import TemplateBase
from bag.layout.tech import TechInfoConfig
from bag.layout.util import BBox

from .analog_mos.planar import MOSTechPlanarGeneric
from .resistor.planar import ResTechPlanarGeneric
from .layer_parameters import layer_parameters
from .mos_parameters import mos_parameters
from .via_parameters import via_parameters
from .resistor_parameters import resistor_parameters


class BAG2_technology_definition(TechInfoConfig):

    def __init__(self, process_params):
        super().__init__(self.process_config, process_params)
        # These are the original settings
        process_params['layout']['mos_tech_class'] = MOSTechPlanarGeneric(self.process_config, self)
        process_params['layout']['res_tech_class'] = ResTechPlanarGeneric(self.process_config, self)

    @property
    def process_config(self):
        """ This is an property that parses through all the process parameters,
        assigns them to Dictionary '_config', and returns that dict.
        """
        if not hasattr(self, '_config') or not hasattr(self, '_process_config'):
            # _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(_yaml_file, 'r') as content:
            # self._process_config = yaml.load(content, Loader=yaml.FullLoader )

            # Initialize the comnpatibility Dict, originally read from Yaml file
            self._process_config = {}
            layers = layer_parameters()
            # Update the dictionary by copying the layers dict to it
            self._process_config.update(layers.property_dict)
            mos = mos_parameters()
            via = via_parameters()
            resistor = resistor_parameters()
            self._process_config['mos'] = mos.property_dict
            # Via parameters should be isolated inside via class.
            # These are for compatibility
            self._process_config['via'] = via.property_dict
            self._process_config['via_name'] = via.property_dict['via_name']
            self._process_config['via_id'] = via.property_dict['via_id']
            self._process_config['resistor'] = resistor.property_dict
        self._config = self._process_config
        return self._process_config

    @property
    def grid_opts(self):
        """Dict : Global routing track definition options for the process.
        Utilized by bag_ecd.bag_desing.routing_grid.
        'num' is a placeholder for a numeric value.

        Keys:
        -----
        bot_dir :  y|x , Direction fo the bottom routing layer
        layers  : list of available layer numbers
        spaces  : default spacing of the routing layers
        widths  : default widths of routing layers
        with_override: Dict of Dicts. Purpose unclear.
        """
        if not hasattr(self, '_grid_opts'):
            self._grid_opts = {
                'bot_dir': 'y',
                #          L1,       M1,     M2,         M3,          M4,     M5
                'layers': [1, 2, 3, 4, 5, 6],
                'spaces': [0.17, 0.14, 0.14, 0.3, 0.3, 1.6],  # (Given in µm)
                'widths': [0.17, 0.28, 0.28, 0.33, 0.33, 1.6],  # (Given in µm) #Default routing widths for each layer
                # design rule [  ,    ,        , m3.1/via3.5,     ,        ]
                # NOTE: some of these values are updated in set_layout_info() {while AnalogBaseInfo object is created}
                'width_override': {}
            }
            return self._grid_opts
        else:
            return self._grid_opts

    @property
    def min_lch(self):
        """ Minimum lch for transistors. You may generate designs using multiples of this

        """
        if not hasattr(self, '_min_lch'):
            self._min_lch = 150.0e-9
            return self._min_lch
        else:
            return self._min_lch

    @property
    def config(self):
        """Contains all parameters defined in BAG2 primary configuration file 'tech_params.yaml
        """
        if not hasattr(self, '_config'):
            _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            with open(_yaml_file, 'r') as content:
                self._config = yaml.load(content, Loader=yaml.FullLoader)
        return self._config

    @config.setter
    def config(self, val):
        # This setter is here for compatibility
        # Enables redefinition of config, but it is redefined most often from the same yaml file
        self._config = val

    # Methods
    def get_transistor_primitives(self, **kwargs):
        """
        Maps given flavor and type to a name of a vendor primitive
        this can be used to enable use of vendor primitives in e.g.
        AnalogBase

        Parameters
        ----------
        flavor: str, Default 'lvt'
        type: str, Default 'nch'

        Example
        -------
        self.get_transistor_primitives(flavor='lvt', type='nch')
        """
        flavor = kwargs.get('flavor', 'lvt')
        type = kwargs.get('type', 'nch')

        # Dictionary of cell names
        primdict = {
            'nch': {
                'lvt': 'namefortheprimitive'
            },
            'pch': {
                'lvt': 'namefortheprimitive'
            }
        }
        return primdict[type][flavor]

    def get_metal_em_specs(self, layer_name, w, l=-1, vertical=False, **kwargs):
        """Yes, we need to document this too
        """
        metal_type = self.get_layer_type(layer_name)
        idc = self._get_metal_idc(metal_type, w, l, vertical, **kwargs)
        irms = self._get_metal_irms(layer_name, w, **kwargs)
        ipeak = float('inf')
        return idc, irms, ipeak

    # In the following methods I have replaced all values 
    def get_metal_idc_factor(self, mtype, w, l):
        """Yes, we need to document this too
        """
        if l < 0:
            l = float('inf')
        # TODO: clean this table for sky 130, this is simply a guess .. Thomas, Marko - what do these values mean?
        if mtype in {'li1', 'met1', 'met2', 'met3', 'met4', 'met5'}:
            if l <= 5.0:
                return 4
            elif w >= 0.1:
                return 2
            else:
                return 1
        else:
            raise Exception('Unknown metal type %s' % mtype)

    def _get_metal_idc(self, metal_type, w, l, vertical, **kwargs):
        """Yes, we need to document this too
        """
        if vertical:
            # no documentation
            return float('inf')

        # TODO: clean this table for sky 130, this is simply a guess .. Thomas, Marko - what do these values mean?
        w_shrink = 1.0
        if metal_type in {'li1','met1', 'met2', 'met3', 'met4'}: #Changed metal_types from {1,2,3,4} -> {'met1', 'met2', 'met3', 'met4'} for sky130
            inorm, woff = 0.996, 0.003
        else:
            raise Exception('Unknown metal type %s' % metal_type)
        idc = inorm * self.get_metal_idc_factor(metal_type, w, l) * (w * w_shrink - woff)
        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, metal_type) * idc * 1e-3

    def _get_metal_irms(self, layer_name, w, **kwargs):
        """Yes, we need to document this too
        """
        layer_id = self.get_layer_id(layer_name)
        # TODO: no idea what to put here ...
        b = 0.04
        wscale = 1.0
        if layer_id == 1:
            k, wo, a = 20.0, 0.003, 0.3
        elif layer_id == 2:
            k, wo, a = 20.0, 0.003, 0.3
        elif layer_id == 3:
            k, wo, a = 20.0, 0.003, 0.3
        elif layer_id == 4:
            k, wo, a = 20.0, 0.003, 0.3
        elif layer_id == 5:
            k, wo, a = 20.0, 0.003, 0.3
        else:
            raise ValueError('unknown metal layer name: ' + layer_name)

        irms_dt = kwargs.get('rms_dt', self.irms_dt)
        irms_ma = (k * irms_dt * (w * wscale - wo) ** 2 * (w * wscale - wo + a) / (w * wscale - wo + b)) ** 0.5
        return irms_ma * 1e-3

    def _get_via_idc(self, vname, via_type, bm_type, tm_type,
                     bm_dim, tm_dim, array, **kwargs):
        # TODO: what to put here?
        if bm_dim[0] > 0:
            bf = self.get_metal_idc_factor(bm_type, bm_dim[0], bm_dim[1])
        else:
            bf = 1.0

        if tm_dim[0] > 0:
            tf = self.get_metal_idc_factor(tm_type, tm_dim[0], tm_dim[1])
        else:
            tf = 1.0

        factor = min(bf, tf)
        if vname in {'L1M1_C', 'M1M2_C', 'M2M3_C', 'M3M4_C', 'M4M5_C'}:
            if via_type == 'square':
                if factor == 2 and not array:
                    # factor = 2 only works for array
                    factor = 1
                idc = 0.05 * factor
            elif via_type in {'hrect', 'vrect'}:
                idc = 0.1 * factor
            else:
                raise ValueError('Unsupported vtype %s' % via_type)
        else:
            raise ValueError('Unsupported vname %s and bmtype %s' % (vname, bm_type))

        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, bm_type) * idc * 1e-3

    def get_via_em_specs(self, via_name, bm_layer, tm_layer, via_type='square',
                         bm_dim=(-1, -1), tm_dim=(-1, -1), array=False, **kwargs):
        bm_type = self.get_layer_type(bm_layer)
        tm_type = self.get_layer_type(tm_layer)
        idc = self._get_via_idc(via_name, via_type, bm_type, tm_type, bm_dim,
                                tm_dim, array, **kwargs)
        # via do not have AC current specs
        irms = float('inf')
        ipeak = float('inf')
        return idc, irms, ipeak

    def get_res_em_specs(self, res_type, w, l=-1, **kwargs):
        """Yes, we need to document this too
        """
        # TODO: what to put here?
        if res_type in {'high_r', 'standard', 'high_speed'}:
            wscale = 1.0
            idc_temp = kwargs.get('dc_temp', self.idc_temp)
            idc_scale = self.get_idc_scale_factor(idc_temp, '', is_res=True)
            idc = 0.4e-3 * w * wscale * idc_scale

            irms_dt = kwargs.get('rms_dt', self.irms_dt)
            irms = 1e-3 * (0.00908 * irms_dt * w * wscale * (w * wscale + 0.266)) ** 0.5
            ipeak = 2.0e-3 * w * wscale
            return idc, irms, ipeak
        else:
            raise ValueError('Unsupported resistor type: %s' % res_type)

    def add_cell_boundary(self, template: TemplateBase, box: BBox):
        """Yes, we need to document this too
        """
        pass

    def draw_device_blockage(self, template: TemplateBase):
        """Yes, we need to document this too
        """
        od_box = BBox.get_invalid_bbox()
        for od_purp in 'drawing':
            od_box = od_box.merge(template.get_rect_bbox(('diff', od_purp)))
        po_box = BBox.get_invalid_bbox()
        for po_purp in 'drawing':
            po_box = po_box.merge(template.get_rect_bbox(('poly', po_purp)))

        # TODO: what is this for?
        # intersec_box = od_box.intersect(po_box)
        # blk_box = intersec_box.expand(dx=40, dy=40, unit_mode=True)
        # template.add_rect(('POBLK', 'dummy'), blk_box)
        # template.add_rect(('ODBLK', 'dummy'), blk_box)

    def get_via_arr_enc(self, vname, vtype, mtype, mw_unit, is_bot) -> \
            Tuple[Optional[List[Tuple[int, int]]], Optional[Callable[[int, int], bool]]]:
        """Yes, we need to document this too
        """
        return None, None
